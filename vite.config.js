import { defineConfig } from 'vite'
import laravel from 'laravel-vite-plugin'
import vue from '@vitejs/plugin-vue2'


export default defineConfig({
  plugins: [
    vue({
      template: {
        transformAssetUrls: {
          base: null,
          includeAbsolute: false
        }
      }
    }),
  ],
  build: {
    lib: {
      entry: './resources/js/components.js',
      name: 'SautorAttendance',
      fileName: 'sautor-attendance',
    },
    outDir: './public',
    rollupOptions: {
      external: ['vue'],
      output: {
        globals: {
          vue: "Vue"
        },
        entryFileNames: '[name].js',
        chunkFileNames: '[name].js',
        assetFileNames: '[name].[ext]'
      }
    }
  }
})
