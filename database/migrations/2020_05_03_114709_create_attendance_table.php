<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('att_attendance', function (Blueprint $table) {
            $table->id();
            $table->integer('grupo_id')->unsigned();
            $table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('cascade');
            $table->integer('pessoa_id')->unsigned();
            $table->foreign('pessoa_id')->references('id')->on('pessoas')->onDelete('cascade');
            $table->date('attended_at');
            $table->timestamps();
        });

        if (Schema::hasTable('presencas')) {
            $old_attendance = DB::table('presencas')->get();
            $updated_attendance = $old_attendance->map(function ($attendance) {
                return [
                    'id' => $attendance->id,
                    'grupo_id' => $attendance->grupo_id,
                    'pessoa_id' => $attendance->pessoa_id,
                    'attended_at' => $attendance->data,
                    'created_at' => $attendance->created_at,
                    'updated_at' => $attendance->updated_at,
                ];
            });
            DB::table('att_attendance')->insert($updated_attendance->all());

            Schema::dropIfExists('presencas');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('att_attendance');
    }
}
