# Sautor Attendance

Extensão de presenças para o sistema Sautor.

## Instalação

Instalar via Composer:

````sh
composer require sautor/attendance
````

No ficheiro `resources/js/app.js`:

````js
// Na linha 4, importar também a função registerComponent
import initVue, { registerComponent } from '@sautor/core/resources/js/bootstrap/vue'

// Importar o componente
import Presencas from '@sautor/attendance/resources/js/components/Presencas'

// Antes da chamada à função initVue, registar o componente
registerComponent(Vue, Presencas)
````

Para correr a migração:
````sh
php artisan migrate
````
