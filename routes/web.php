<?php

use Sautor\Attendance\Controllers\AttendanceController;

Sautor\groupRoute(function () {
    Route::get('presencas', AttendanceController::class)->name('attendance')->middleware('auth');
});
