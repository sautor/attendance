<?php

use Sautor\Attendance\Controllers\AttendanceApiController;

Sautor\groupApiRoute(function () {
    Route::put('presencas', [AttendanceApiController::class, 'update'])->name('attendance.update');
    Route::get('presencas/calendar', [AttendanceApiController::class, 'calendar'])->name('attendance.calendar');
    Route::get('presencas/day', [AttendanceApiController::class, 'day'])->name('attendance.day');
});
