@extends('layouts.group-admin')

@section('group-content')
    <div class="group-page__header">
        <h1 class="group-page__header__title">
            Presenças
        </h1>
    </div>

        <sautor-attendance
                calendar-url="{{ $grupo->route('api.attendance.calendar') }}"
                day-url="{{ $grupo->route('api.attendance.day') }}"
                update-url="{{ $grupo->route('api.attendance.update') }}"
                grupo-id="{{ $grupo->id }}"
        >
            <div class="text-center text-primary-500">
                <span class="far fa-circle-notch fa-spin fa-4x"></span>
            </div>
        </sautor-attendance>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/attendance/style.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('addons/attendance/components.js') }}" type="module"></script>
@endpush
