import vueCustomElement from 'vue-custom-element'
import Presencas from './components/Presencas.vue'
import Modal from '../../vendor/sautor/core/resources/js/components/Modal.vue'
import modals from '../../vendor/sautor/core/resources/js/mixins/modals.js'

import '../scss/styles.scss'

window.Vue.use(vueCustomElement);

window.Vue.component('Modal', Modal)

window.Vue.customElement('sautor-attendance', Presencas, {
    beforeCreateVueInstance (rootElement) {
        rootElement.mixins = [modals]
        return rootElement
    }
})
