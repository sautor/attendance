<?php

namespace Sautor\Attendance\Controllers;

use DB;
use Illuminate\Http\Request;
use Sautor\Attendance\Models\Attendance;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;

class AttendanceApiController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Throwable
     */
    public function update(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);

        if (! $request->has(['date', 'attendance'])) {
            abort(400, 'Missing required parameters.');

            return;
        }

        DB::transaction(function () use ($request, $grupo) {
            $attendance = Attendance::where('grupo_id', $grupo->id)->where('attended_at', $request->date)->get();

            foreach ($attendance as $singleAttendance) {
                if (! in_array($singleAttendance->pessoa_id, $request->attendance)) {
                    $singleAttendance->delete();
                }
            }

            foreach ($request->attendance as $pessoa_id) {
                if ($attendance->contains('pessoa_id', $pessoa_id)) {
                    continue;
                }
                Attendance::create([
                    'grupo_id' => $grupo->id,
                    'pessoa_id' => $pessoa_id,
                    'attended_at' => $request->date,
                ]);
            }
        });

        return response('', 200);
    }

    public function calendar(Request $request, Grupo $grupo)
    {
        $this->validate($request, [
            'from' => 'required|date',
            'to' => 'required|date',
        ]);
        $this->authorize('update', $grupo);

        $start_date = $request->get('from');
        $end_date = $request->get('to');

        $attendance = Attendance::whereIn('pessoa_id', $grupo->inscritos()->select('pessoas.id'))
            ->where('attended_at', '>=', $start_date)->where('attended_at', '<=', $end_date)
            ->select(['grupo_id', 'attended_at'])->distinct()->get();

        $groups = Grupo::whereIn('id', $attendance->pluck('grupo_id')->unique()->toArray())->with('pai')->get();

        return $attendance->groupBy('grupo_id')->map(function ($item, $grupo_id) use ($groups) {
            $g = $groups->where('id', $grupo_id)->first();
            $color = $g->cor;

            while (empty($color) && $g->pai) {
                $g = $g->pai;
                $color = $g->cor;
            }

            return [
                'group_id' => $grupo_id,
                'dates' => $item->pluck('attended_at')->map(fn ($date) => (new \DateTime($date))->format('Y-m-d')),
                'color' => $color,
            ];
        })->values();
    }

    public function day(Request $request, Grupo $grupo)
    {
        $this->validate($request, [
            'date' => 'required|date',
        ]);
        $this->authorize('update', $grupo);

        $date = $request->get('date');

        $inscritos = $grupo->inscritos()->get();

        $attendance_per_group = Attendance::whereIn('pessoa_id', $grupo->inscritos()->select('pessoas.id'))
            ->where('attended_at', $date)->get()->groupBy('grupo_id');

        $groups = Grupo::whereIn('id',
            $attendance_per_group->keys()->toArray())->orderBy('ordem')->orderBy('nome')->with('inscritos')->get();

        $groups_with_attendance = $groups->map(fn ($group) => [
            'group' => $group->only(['id', 'nome', 'nome_curto', 'logo', 'cor']),
            'attendance' => $group->inscritos->intersect($inscritos)->mapWithKeys(fn ($inscrito) => [
                $inscrito->id => $attendance_per_group->get($group->id)->contains('pessoa_id', $inscrito->id),
            ]),
        ]);

        $self_group = $groups_with_attendance->where('group.id', $grupo->id)->first();

        return [
            'self' => $self_group ? $self_group['attendance'] : null,
            'others' => $groups_with_attendance->where('group.id', '!=', $grupo->id)->values(),
        ];
    }
}
