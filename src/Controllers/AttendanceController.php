<?php

namespace Sautor\Attendance\Controllers;

use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;

class AttendanceController extends Controller
{
    public function __invoke(Grupo $grupo)
    {
        return view('attendance::index', compact('grupo'));
    }
}
