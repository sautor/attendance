<?php

namespace Sautor\Attendance;

use Illuminate\Support\ServiceProvider;
use Sautor\Core\Services\Addons\Addon;

class AttendanceServiceProvider extends ServiceProvider
{
    private Addon $addon;

    /**
     * AttendanceServiceProvider constructor.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->addon = Addon::create('attendance', 'Presenças', 'user-check', 'Extensão para controlo de presenças dos inscritos de um grupo. As presenças podem também ser vistas por responsáveis de outros grupos onde a pessoa esteja inscrita.')
            ->setEntryRouteForGroup('attendance')
            ->withAssets();
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'sautor');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'attendance');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');

        $addonService = $this->app->make('AddonService');
        $addonService->register($this->addon);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
